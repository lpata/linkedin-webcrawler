import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Integration {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Integration obj=new Integration();
			//*************************************************************************************
			//Reading Alumni Names from excel file
			File excel =  new File ("C:/Users/LokeshwerReddy/Desktop/fall'15/Ind Study/data.xlsx");
			FileInputStream fis = new FileInputStream(excel);
			XSSFWorkbook wb = new XSSFWorkbook(fis);
			XSSFSheet ws = wb.getSheetAt(0);
			int rowNum = ws.getLastRowNum() + 1;
			int colNum = ws.getRow(0).getLastCellNum();
			for(int i = 1; i <rowNum; i++){
				XSSFRow row = ws.getRow(i);
				XSSFCell cell = row.getCell(0);
				String value=null;
				if(cell!=null)
					value = cell.toString(); 
				XSSFCell cell2 = row.getCell(1);
				if(cell2!=null)
				{
					value=value+" "+cell2.toString();
				}
				value=value.replace(" ", "%20");
				//*************************************************************************************
				//Searching Google for each profile in LinkedIn
				Set<String> result = obj.getDataFromGoogle(value+"%20linkedin%20unm");
				for(String temp : result){
					//System.out.println(temp);
					Connection con=Jsoup.connect(temp);
				//*************************************************************************************
				//Crawling each linkedin profile
					Document doc=con.get();
					//************Name****************
					Element eleName=doc.getElementsByClass("full-name").first();
					if(eleName!=null)
					{
						System.out.println("Full Name:"+eleName.text());
					}
					else
					{
						eleName=doc.getElementById("name");
						if(eleName!=null)
						{
							System.out.println("Name:"+eleName.text());
						}
					}
					Element eleGivenName=doc.getElementsByClass("givenname").first();
					if(eleGivenName!=null)
					{
						System.out.println("Given Name:"+eleGivenName.text());
					}
					Element elefamilyName=doc.getElementsByClass("familyname").first();
					if(elefamilyName!=null)
					{
						System.out.println("Family Name:"+elefamilyName.text());
					}
					//**********Title******************
					Element eletitle=doc.getElementsByClass("title").first();
					if(eletitle!=null)
					{
						System.out.println("Title:"+eletitle.text());
					}

					Element eleDemogra=doc.getElementById("demographics");
					if(eleDemogra!=null)
					{
						Elements eleHeader1=eleDemogra.getElementsByTag("dt");
						Elements eleValue=eleDemogra.getElementsByTag("dd");

						for (int i1=0;i1<eleHeader1.size();i1++) {

							System.out.println(eleHeader1.get(i1).text()+":");
							System.out.println(eleValue.get(i1).text());

						}	
					}
					Element ele=doc.getElementsByTag("table").first();
					if(ele!=null)
					{
						Elements eletr=ele.getElementsByTag("tr");
						for (Iterator iterator1 = eletr.iterator(); iterator1.hasNext();) {
							Element element = (Element) iterator1.next();
							Element eleHeader=element.getElementsByTag("th").first();
							if(eleHeader!=null)
							{
								System.out.println(eleHeader.text()+":");
								Element eleBody=element.getElementsByTag("td").first();
								Elements eleList=eleBody.getElementsByTag("li");
								for (Element eleEach : eleList) {
									if(eleEach!=null)
										System.out.println(eleEach.text());
								}							
							}						
						}
					}
				}
			}

		}catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private Set<String> getDataFromGoogle(String query) {

		Set<String> result = new HashSet<String>();	
		String request = "https://www.google.com/search?q=" + query + "&num=20";
		System.out.println("Sending request..." + request);

		try {

			// need http protocol, set this as a Google bot agent :)
			Document doc = Jsoup
					.connect(request)
					.userAgent(
							"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
					.timeout(5000).get();

			// get all links
			Elements links = doc.select("a[href]");
			for (Element link : links) {

				String temp = link.attr("href");		
				if(temp.startsWith("/url?q=")){
					String req,nreq;
					if(temp.contains("https://www.linkedin.com/pub/"))
					{
						req=temp.substring(temp.lastIndexOf("https"));
						req=req.substring(0, req.lastIndexOf("&sa"));
						result.add(req);
					}
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

}
