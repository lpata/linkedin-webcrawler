
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Iterator;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GoogleCrawler {

	private static Pattern patternDomainName;
	private Matcher matcher;
	private static final String DOMAIN_NAME_PATTERN 
	= "([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}";
	static {
		patternDomainName = Pattern.compile(DOMAIN_NAME_PATTERN);
	}

	public static void main(String[] args) {
		//BufferedWriter writer = null;
		GoogleCrawler obj = new GoogleCrawler();
		HashMap<String, Integer> hm=new HashMap<String, Integer>();
		HashSet<String> hmName=new HashSet<String>();
		HashSet<String> hmSchool=new HashSet<String>();
		hmSchool.add("UNM");
		hmSchool.add("The University of New Mexico");
		hmSchool.add("University of New Mexico");
		DataInputStream dis = new DataInputStream(System.in);
		//String validationFile = null;
		String data1,output;
		try {
			int k=0;
			//
			System.out.println("Enter the input file name with complete path(Eg:C:fall'15/Ind Study/data.xlsx)");
			data1 = dis.readLine();
			//File excel =  new File ("C:/Users/LokeshwerReddy/Desktop/fall'15/Ind Study/data.xlsx");
	        FileInputStream fis = new FileInputStream(data1);
	        XSSFWorkbook wb = new XSSFWorkbook(fis);
	        XSSFSheet ws = wb.getSheetAt(0);

	        int rowNum = ws.getLastRowNum() + 1;
	        int colNum = ws.getRow(0).getLastCellNum();
	        //String [][] data = new String [rowNum] [colNum];
	        
	        System.out.println("Enter the created output file name with complete path(Eg:C:fall'15/Ind Study/output.xlsx)");
	        output=dis.readLine();
	        FileInputStream fis2 = new FileInputStream(output);
			XSSFWorkbook workbook = new XSSFWorkbook (fis2);
			XSSFSheet sheet = workbook.getSheetAt(0);

	        for(int i = 1; i <rowNum; i++){
	            XSSFRow rowr = ws.getRow(i);
	            
	                //for (int j = 0; j < colNum; j++){
	                    XSSFCell cell = rowr.getCell(0);
	                    String value=null;
	                   if(cell!=null)
	                   value = cell.toString(); 
	                    XSSFCell cell2 = rowr.getCell(1);
	                    if(cell2!=null)
	                    {
	                    	value=value+" "+cell2.toString();
	                    }
	                    value=value.replace(" ", "%20");
	                    //System.out.println ("the value is " + value);
			Set<String> result = obj.getDataFromGoogle(value+"%20linkedin%20university%20of%20new%mexico");
			boolean bfirst=true;
			for(String temp : result){
				boolean bIsUnm=false;
				try{
			//System.out.println(temp);
			k++;
			//String link="https://www.linkedin.com/pub/susheel-reddy-gurudu/9b/280/82b";
			Connection con=Jsoup.connect(temp);
			
			//Create First Row
			XSSFRow row1 = sheet.createRow(0);
			XSSFRow row = sheet.createRow(k);

			Document doc=con.get();
			//writer = new BufferedWriter( new FileWriter("xml.txt"));
			//writer.write(doc.toString());
			//************Name****************
			Element eleName=doc.getElementsByClass("full-name").first();
			
			int f=0;
			int tempr=0;
			if(eleName!=null)
			{
				//System.out.println("Full Name:"+eleName.text());
				if(bfirst)
				{
					XSSFCell r1c1 = row1.createCell(f);
					r1c1.setCellValue("Name");
					f++;
				}
				
			}
			else
			{
				eleName=doc.getElementById("name");
				if(eleName!=null)
				{
					//System.out.println("Name:"+eleName.text());
					if(bfirst)
					{
						XSSFCell r1c1 = row1.createCell(f);
						r1c1.setCellValue("Name");
						f++;
					}
					/*XSSFCell r2c1 = row.createCell(tempr);
					r2c1.setCellValue(eleName.text());
					tempr++;*/
					
				}
			}
			if(eleName==null || eleName.equals(""))
			{
				k--;
				continue;
			
			}
			else if(eleName.text().equalsIgnoreCase("LinkedIn"))
			{
				k--;
				continue;
			}
			else if(hmName.contains(eleName.text()))
			{
				k--;
				continue;
			}
			else
			{
				hmName.add(eleName.text());
			XSSFCell r2c1 = row.createCell(tempr);
			r2c1.setCellValue(eleName.text());
			tempr++;
			}
			
			/*Element eleGivenName=doc.getElementsByClass("givenname").first();
			if(eleGivenName!=null)
			{
				System.out.println("Given Name:"+eleGivenName.text());
			}
			Element elefamilyName=doc.getElementsByClass("familyname").first();
			if(elefamilyName!=null)
			{
				System.out.println("Family Name:"+elefamilyName.text());
			}*/
			//**********Title******************
			Element eletitle=doc.getElementsByClass("title").first();
			if(eletitle!=null)
			{
				//System.out.println("Title:"+eletitle.text());
				if(bfirst)
				{
					XSSFCell r1c1 = row1.createCell(f);
					r1c1.setCellValue("Title");
					f++;
				}
				XSSFCell r2c1 = row.createCell(tempr);
				r2c1.setCellValue(eletitle.text());
				tempr++;
			}
			if(bfirst)
			{
				XSSFCell r1c1 = row1.createCell(f);
				r1c1.setCellValue("Location");
				hm.put("Location", f);
				f++;
				XSSFCell r1c2 = row1.createCell(f);
				r1c2.setCellValue("Industry");
				hm.put("Industry", f);
				f++;
				XSSFCell r1c3 = row1.createCell(f);
				r1c3.setCellValue("Current");
				hm.put("Current", f);
				f++;
				XSSFCell r1c4 = row1.createCell(f);
				r1c4.setCellValue("Previous");
				hm.put("Previous", f);
				f++;
				XSSFCell r1c5 = row1.createCell(f);
				r1c5.setCellValue("Education");
				hm.put("Education", f);
				f++;
			}
			Element eleDemogra=doc.getElementById("demographics");
			if(eleDemogra!=null)
			{
				Elements eleHeader1=eleDemogra.getElementsByTag("dt");
				Elements eleValue=eleDemogra.getElementsByTag("dd");

				for (int i1=0;i1<eleHeader1.size();i1++) {

					//System.out.println(eleHeader1.get(i1).text()+":");
					/*if(bfirst)
					{
						XSSFCell r1c1 = row1.createCell(f);
						r1c1.setCellValue(eleHeader1.get(i).text());
						
					}*/
					int temp1=0;
					if(eleHeader1.get(i1)!=null)
					{
					temp1=hm.get(eleHeader1.get(i1).text());
					if(hmSchool.contains(eleValue.get(i1).text()))
					{
						bIsUnm=true;
					}
					}
					if(temp1>0)
					{
					//System.out.println(eleValue.get(i1).text());
					
					XSSFCell r2c1 = row.createCell(temp1);
					r2c1.setCellValue(eleValue.get(i1).text());
					//f++;
					}
				}	
			}
			tempr=tempr+5;
			Element ele=doc.getElementsByTag("table").first();
			if(ele!=null)
			{
				Elements eletr=ele.getElementsByTag("tr");
				for (Iterator iterator1 = eletr.iterator(); iterator1.hasNext();) {
					Element element = (Element) iterator1.next();
					Element eleHeader=element.getElementsByTag("th").first();
					if(eleHeader!=null)
					{
						//System.out.println(eleHeader.text()+":");
						/*if(bfirst)
						{
							XSSFCell r1c1 = row1.createCell(f);
							r1c1.setCellValue(eleHeader.text());
							
						}*/
						
						Element eleBody=element.getElementsByTag("td").first();
						Elements eleList=eleBody.getElementsByTag("li");
						String strAll=null;
						for (Element eleEach : eleList) {
							if(eleEach!=null)
							{
								if(hmSchool.contains(eleEach.text()))
								{
									bIsUnm=true;
								}
								//System.out.println(eleEach.text());
								if(strAll==null)
								{
								strAll=eleEach.text();
								}
								else
								{
								strAll=strAll+eleEach.text();	
								}
							}
						}
						int temp1=0;
						if(eleHeader.text()!=null)
						{
							//System.out.println(eleHeader.text());
							if(hm.get(eleHeader.text())!=null)
							{
						temp1=hm.get(eleHeader.text());
							}
						}
						if(temp1>0)
						{
						XSSFCell r2c1 = row.createCell(temp1);
						r2c1.setCellValue(strAll);
						}
						//f++;
					}						
				}
				//}
				//System.out.println(ele);
			} 
			if(bfirst)
			{
				XSSFCell r1c1 = row1.createCell(f);
				r1c1.setCellValue("Experience(In Detail)");
				f++;
			}
			Element eleEd=doc.getElementById("experience");
			if(eleEd!=null)
			{
				//System.out.println("first exp");
				Elements eleList=eleEd.getElementsByTag("li");
				String strExp="1.";
				int c=1;
				for (Element eleEach: eleList) {
					Element eleTitle=eleEach.getElementsByClass("item-title").first();
					if(eleTitle!=null)
					{						
						//System.out.println(eleTitle.text());
						if(strExp==null)
							strExp=eleTitle.text();
						else
							strExp=strExp+eleTitle.text();
					}
					Element eleSubTitle=eleEach.getElementsByClass("item-subtitle").first();
					if(eleSubTitle!=null)
					{
						//System.out.println(eleSubTitle.text());
						if(hmSchool.contains(eleSubTitle.text()))
						{
							bIsUnm=true;
						}
						if(strExp==null)
							strExp=eleSubTitle.text();
						else
							strExp=strExp+","+eleSubTitle.text();
					}
						
					Element eleLoc=eleEach.getElementsByClass("location").first();
					if(eleLoc!=null)
					{
						//System.out.println(eleLoc.text());
						if(strExp==null)
							strExp=eleLoc.text();
						else
							strExp=strExp+","+eleLoc.text();
					}					
					Element eleTime=eleEach.getElementsByClass("date-range").first();
					if(eleTime!=null)
					{
						//System.out.println(eleTime.text());
						if(strExp==null)
							strExp=eleTime.text();
						else
							strExp=strExp+","+eleTime.text();
					}
					if(c<eleList.size())
					{
					c++;
					strExp=strExp+"\n\n"+c+".";
					}
					
				//System.out.println("1");	
				}
				XSSFCell r2c1 = row.createCell(tempr);
				r2c1.setCellValue(strExp);
				tempr++;
				//System.out.println("2");
			}
			if(eleEd==null)
			{
				eleEd=doc.getElementById("background-experience");
				if(eleEd!=null)
				{
					//System.out.println("second exp");
				Elements eleReq=eleEd.select("div[id~=^experience-[0-9]+$]");
				//Elements eleList=eleEd.getElementsByTag("div");
				int c=1;
				String strEdu="1.";
				for (Element eleEach: eleReq) {
					//System.out.println("In for***********************"+eleEach);
					Element eleTit=eleEach.getElementsByAttributeValue("title", "Learn more about this title").first();
					if(eleTit!=null)
					{
					//System.out.println(eleTit.text());
					strEdu=strEdu+eleTit.text();
					}
					Element eleJob=eleEach.getElementsByAttributeValue("dir", "auto").first();
					if(eleJob!=null)
					{
						if(hmSchool.contains(eleJob.text()))
						{
							bIsUnm=true;
						}
					//System.out.println(eleJob.text());
					strEdu=strEdu+","+eleJob.text();
					}
					Element eleTime=eleEach.getElementsByClass("experience-date-locale").first();
					//eleTime=(Element) eleTime.removeAttr("class");
					//System.out.println(eleTime);


					Element eleLoc=eleEach.getElementsByClass("locality").first();
					String strTim="";
					if(eleTime!=null & eleLoc!=null)
					{
					strTim=eleTime.text().replace(eleLoc.text(), "");
					}
					if(strTim!=null)
					{
						//System.out.println(strTim);
						strEdu=strEdu+","+strTim;
					}
					else if(eleLoc!=null)
					{
						//System.out.println(eleLoc.text());
						strEdu=strEdu+","+eleLoc.text();
					}
					/*Element eleTitle=eleEach.getElementsByAttribute("title").first();
					System.out.println(eleTitle.text());*/
					if(c<eleReq.size())
					{
					c++;
					strEdu=strEdu+"\n\n"+c+".";
					}
					
					
				}
				XSSFCell r2c1 = row.createCell(tempr);
				r2c1.setCellValue(strEdu);
				tempr++;
			}
			}	
			Element eleEdu=doc.getElementById("education");
			
			if(bfirst)
			{
				XSSFCell r1c1 = row1.createCell(f);
				r1c1.setCellValue("School(In Detail)");
				f++;
			}
			//System.out.println(eleEdu);
			if(eleEdu!=null)
			{
				//System.out.println("Yes I am here");
				Elements eleSchools=doc.getElementsByClass("school");
				int c=1;
				String strEdu="1.";
				//System.out.println("first");
				for(Element eleEach: eleSchools)
				{
					//System.out.println(eleEach);
					Element eleTitle=eleEach.getElementsByClass("item-title").first();
					if(eleTitle!=null)
					{
						if(hmSchool.contains(eleTitle.text()))
						{
							bIsUnm=true;
						}
					//System.out.println(eleTitle.text());
					strEdu=strEdu+eleTitle.text();
					}
					Element eleSubTitle=eleEach.getElementsByClass("item-subtitle").first();
					if(eleSubTitle!=null)
					{
					//System.out.println(eleSubTitle.text());
					strEdu=strEdu+","+eleSubTitle.text();
					}
					Element eleDate=eleEach.getElementsByClass("date-range").first();
					if(eleDate!=null)
					{
					//System.out.println(eleDate.text());
					strEdu=strEdu+","+eleDate.text();
					}
					if(c<eleSchools.size())
					{
					c++;
					strEdu=strEdu+"\n\n"+c+".";
					}
					//System.out.println("first");
				}
				XSSFCell r2c1 = row.createCell(tempr);
				r2c1.setCellValue(strEdu);
				tempr++;
			}
			if(eleEdu==null)
			{
				//System.out.println("second");
				eleEdu=doc.getElementById("background-education");
				//System.out.println(eleEdu);
				if(eleEdu!=null)
				{
				Elements eleSchools=eleEdu.select("div[id~=^education-[0-9]+$]");
				int c=1;
				String strEdu="1.";
				for(Element eleEach: eleSchools)
				{
					//System.out.println(eleEach);
					Element eleTitle=eleEach.getElementsByAttributeValue("class","summary fn org").first();
					if(eleTitle!=null)
					{
						if(hmSchool.contains(eleTitle.text()))
						{
							bIsUnm=true;
						}
					//System.out.println(eleTitle.text());
					strEdu=strEdu+eleTitle.text();
					}
					Element eleSubTitle=eleEach.getElementsByClass("degree").first();
					if(eleSubTitle!=null)
					{
					//System.out.println(eleSubTitle.text());
					strEdu=strEdu+","+eleSubTitle.text();
					}
					Element eleDate=eleEach.getElementsByClass("education-date").first();
					if(eleDate!=null)
					{
					//System.out.println(eleDate.text());
					strEdu=strEdu+","+eleDate.text();
					}
					if(c<eleSchools.size())
					{
					c++;
					strEdu=strEdu+"\n\n"+c+".";
					}
					//System.out.println("second");
					
				}
				XSSFCell r2c1 = row.createCell(tempr);
				r2c1.setCellValue(strEdu);
				tempr++;
			}
			}
			if(bfirst)
			{
				XSSFCell r1c1 = row1.createCell(f);
				r1c1.setCellValue("LinkedIn Link");
				f++;
			}
			XSSFCell r2c1 = row.createCell(9);
			r2c1.setCellValue(temp);
			tempr++;
			if(!bIsUnm)
			{
				k--;
				continue;
			
			}
			
			
			System.out.println("..............");
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				//System.out.println("in this io");
				k=k-1;
				continue;
				//e.printStackTrace();
			}
			finally
			{
				try
				{
					/*if ( writer != null)
						writer.close( );*/
				}
				catch ( Exception e)
				{
				}
			}
			}
			bfirst=false;
	        }
			//System.out.println("out of loop");
			fis2.close();
			FileOutputStream fos =new FileOutputStream(output);
			workbook.write(fos);
			fos.close();
			System.out.println("Successfully written in file");

			//}


		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			//System.out.println("in this io");
			e.printStackTrace();
		}
		finally
		{
			try
			{
				/*if ( writer != null)
					writer.close( );*/
			}
			catch ( Exception e)
			{
			}
		}
		//}
		//System.out.println(result.size());
	}

	public String getDomainName(String url){

		String domainName = "";
		matcher = patternDomainName.matcher(url);
		if (matcher.find()) {
			domainName = matcher.group(0).toLowerCase().trim();
		}
		return domainName;

	}

	private Set<String> getDataFromGoogle(String query) {

		Set<String> result = new HashSet<String>();	
		String request = "https://www.google.com/search?q=" + query + "&num=20";
		//System.out.println("Sending request..." + request);

		try {

			// need http protocol, set this as a Google bot agent :)
			Document doc = Jsoup
					.connect(request)
					.userAgent(
							"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)")
					.timeout(5000).get();

			// get all links
			Elements links = doc.select("a[href]");
			for (Element link : links) {

				String temp = link.attr("href");		
				if(temp.startsWith("/url?q=")){
					//use regex to get domain name
					//result.add(getDomainName(temp));
					String req;
					if(temp.contains("https://www.linkedin.com/"))
					{
						req=temp.substring(temp.lastIndexOf("https"));
						req=req.substring(0, req.lastIndexOf("&sa"));
						result.add(req);
					}
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

}
